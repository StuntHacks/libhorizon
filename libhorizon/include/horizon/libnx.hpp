/**
 * @file libnx.hpp
 * @brief Includes libnx if HORIZON_NO_LIBNX isn't defined
 */
#ifndef HORIZON_NO_LIBNX
    #include <switch.h>
#endif
