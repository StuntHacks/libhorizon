/**
 * @file graphics.hpp
 * @brief Includes all graphics-related files of libhorizon
 */
#ifndef GRAPHICS_HPP
#define GRAPHICS_HPP

#pragma once

#include "color.hpp"
#include "renderTarget.hpp"
#include "screen.hpp"
#include "shader.hpp"
#include "shaderProgram.hpp"
#include "vertex.hpp"

#endif /* end of include guard: GRAPHICS_HPP */
