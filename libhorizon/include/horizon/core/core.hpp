/**
 * @file core.hpp
 * @brief Includes all core files of libhorizon
 */
#ifndef CORE_HPP
#define CORE_HPP

#pragma once

#include "applet.hpp"
#include "input.hpp"
#include "mutex.hpp"
#include "parameter.hpp"
#include "thread.hpp"
#include "time.hpp"

#endif /* end of include guard: CORE_HPP */
